from handlers.pull_requests import get_pull_requests
import unittest
from unittest import mock


class TestState(unittest.TestCase):

    def test_state(self):
        self.assertFalse(get_pull_requests("ope"))
        self.assertListEqual(list(get_pull_requests("open")[0].keys()), ["num", "title", "link"])


class TestRequests(unittest.TestCase):

    @mock.patch('requests.get')
    def test_requests(self, get_mock):
        get_mock.status_code = 200

        mock_response = get_mock()
        mock_response.status_code = 200
        mock_response.json.return_value = [
            {"number": 1,
             "title": "Test title",
             "html_url": "http://test.com",
             "state": "open"}
        ]

        get_mock.return_value.get.return_value = mock_response
        expected_res = [{"num": 1, "title": "Test title", "link": "http://test.com"}]
        res = get_pull_requests("open")
        self.assertEqual(res, expected_res)


if __name__ == "__main__":
    unittest.main()
